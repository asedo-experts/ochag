const burgerMenuBtn = document.getElementById("menu-open");

const crossMenuBtn = document.getElementById("menu-close");

const menu = document.querySelector(".header__nav__menu");

burgerMenuBtn.addEventListener('click', () =>{
    menu.classList.add('active');
})

crossMenuBtn.addEventListener('click', () =>{
    menu.classList.remove('active');
})
